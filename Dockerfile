FROM python:3.12.6-alpine3.20

WORKDIR /code

RUN mkdir /archive /repos

COPY ./requirements.* ./

RUN apk add --update-cache bash curl git openssh-client && \
    pip install --no-cache-dir --upgrade --compile -r /code/requirements.txt && \
    rm -rf /var/cache/apk/*

COPY ./app /code/app

EXPOSE 80

# TODO go back to fastapi cli when ready https://github.com/fastapi/fastapi-cli/discussions/13
# needed to go back to uvicorn cli until fastapi supports --forwarded-allow-ips
# ENTRYPOINT ["fastapi", "run", "app/main.py", "--app", "app", "--port", "80"]

ENTRYPOINT ["uvicorn", "--host", "0.0.0.0", "--port", "80", "app.main:app"]
