# Bootstrap Configuration with Ansible

This is a simple text file generator web service which generates config files
based on Ansible Inventory data and templates. It's based on the concepts of
regular Ansible Roles to make it intuitively understandable and it's mainly
meant to generate all the files to bootstrap an OS installation until Ansible
can take over (like ipxe/preseed/kickstart/yast/unattend/... config files,
scripts, etc.).

## Configuration

Some basics like the repositories are configured through environment variables.
All available environment variables are defined in `app/config.py` in
`Settings` (like `log_level` which maps to the env variable
`BOOTSTRAP_LOG_LEVEL`).

Then, the whole configuration and templating happens in `bootstrap.yml` inside
the config repository. See `bootstrap.example.yml` for details!

## Deployment

### Pull Mechanism

There are three ways to keep the repositories up-to-date for this web service
(they can also be combined):

1. Configure hooks on the two repositories (config and inventory), that run the
   respective HTTP request on every commit:

       POST /pull/config # pulls BOOTSTRAP_CONFIG_GIT_URL to /repos/config
       POST /pull/inventory # pulls BOOTSTRAP_INVENTORY_GIT_URL to /repos/inventory

   Note that if you run multiple container replicas, you need to make sure
   `/repos` (and `/archive`) is a shared volume among all of them!

2. Configure a pipeline that (re)deploys this container on every commit to one
   of the repositories (the repos are freshly pulled on the start of the
   container).

3. Enable `BOOTSTRAP_CONFIG_AUTOPULL` / `BOOTSTRAP_INVENTORY_AUTOPULL` (env var
   bools) to make this container pull the repositories before every GET on a
   file (which will slow down the API of course).

### Minimal Example

The minimum you need is a config repository with the following files:

`bootstrap.yml`:

    templates_files: [default.j2]

`defaults/main.yml`:

    # can be empty

`templates/default.j2`:

    Hello {{ bootstrap_hostname }}

With this repository and a read-only token plus access to an ansible inventory
repository, you can start the container:

    docker run --rm --name bootstrap-conf -p 8080:80 \
        --tmpfs /repos:exec \
        --env BOOTSTRAP_CONFIG_GIT_URL="https://user:pass@git.example.com/bootstrap-config.git" \
        --env BOOTSTRAP_INVENTORY_GIT_URL="https://user:pass@git.example.com/inventory.git" \
        manumeter7/bootstrap-conf:latest

You should be able to access the API and the documentation at:
http://localhost:8080/

### Images/Tags

The container images are available with the following tags at:
https://hub.docker.com/r/manumeter7/bootstrap-conf

  - *latest*: The latest stable release (git branch main)
  - *testing*: Every commit before testing (git branch test)

## Development

### Lint Code

    docker build -t bootstrap-conf .

    docker run --rm --entrypoint sh bootstrap-conf:latest -c \
        "pip install pylint &>/dev/null; pylint --rcfile /code/app/.pylintrc /code/app"

### Upgrade Environment

- Check on https://hub.docker.com/r/alpine/helm for new versions and adjust the
  tag in `.gitlab-ci.yml`.

- Check on https://hub.docker.com/_/python for new versions and adjust the tag
  in the `FROM` instruction of `./Dockerfile`. (Use a most specific tag to allow
  reproducable builds.)

- Build container and generate a new requirements file with:

      docker run --rm -v "$(pwd)/requirements.in:/r.in:ro" --entrypoint sh bootstrap-conf:latest -c \
          "pip install pip-tools &>/dev/null; pip-compile -o - /r.in" > ./requirements.txt

      docker build -t bootstrap-conf .
