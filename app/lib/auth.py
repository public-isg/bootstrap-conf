from fastapi import Request, HTTPException, status
from fastapi.security import HTTPBasic
from fastapi.staticfiles import StaticFiles

from app.config import env

security = HTTPBasic()

async def verify_secret(request: Request) -> str:
    credentials = await security(request)

    if env('archive_secret') == '' or env('archive_secret') != credentials.password:
        raise HTTPException(
            status_code=status.HTTP_401_UNAUTHORIZED,
            detail="Incorrect password",
            headers={"WWW-Authenticate": "Basic"},
        )
    return credentials.username

class AuthStaticFiles(StaticFiles):
    def __init__(self, *args, **kwargs) -> None:
        super().__init__(*args, **kwargs)

    async def __call__(self, scope, receive, send) -> None:
        assert scope["type"] == "http"
        request = Request(scope, receive)
        await verify_secret(request)
        await super().__call__(scope, receive, send)
