import os
import shutil

from filelock import FileLock
from git import Repo
from git.exc import GitError

from app.config import env, log

def update(path: str, url: str, branch: str):
    ssh_known_hosts = env("git_ssh_known_hosts_file")
    ssh_key = env("git_ssh_key_file")
    ssh_cmd = f'ssh -o UserKnownHostsFile={ssh_known_hosts} -i {ssh_key}'

    lock = FileLock(f'{path}.lock')
    with lock:
        try:
            repo = Repo(path)
            repo.git.update_environment(GIT_SSH_COMMAND=ssh_cmd)
            assert next(repo.remotes[0].urls) == url
            assert repo.active_branch.name == branch
            assert not repo.is_dirty()
            log('debug', f'Trying to pull repo at {path}')
            repo.remote().pull()
        except (AssertionError, GitError):
            os.makedirs(os.path.dirname(path), exist_ok = True)
            log('debug', f'Cloning git repo to {path}.new')
            repo = Repo.clone_from(
                url,
                f'{path}.new',
                depth = 1,
                branch = branch,
                env = {"GIT_SSH_COMMAND": ssh_cmd}
            )

            if os.path.exists(path):
                log('debug', f'Renaming {path} to {path}.old')
                os.rename(path, f'{path}.old')

            log('debug', f'Renaming {path}.new to {path}')
            os.rename(f'{path}.new', path)

    if os.path.exists(f'{path}.old'):
        log('debug', f'Deleting {path}.old')
        shutil.rmtree(f'{path}.old')

def get_hash(path: str) -> str:
    return Repo(path).head.object.hexsha
