import os
import subprocess
import yaml

from fastapi import HTTPException, status

from app.config import log
from app.lib import ansible
from app.lib import j2

# pylint: disable=global-statement,global-variable-not-assigned
__CONF = None
__DEFAULTS = {
    'template_dir': 'templates',
    'template_archive': None,
    'template_files': [],
    'vars_dir': 'vars',
    'vars_files': ['main.yml'],
    'defaults': 'defaults/main.yml',
    'precheck': '/bin/true',
    'inventory_dir': '.',
    'inventory_host': '{{ bootstrap_hostname }}',
    'inventory_condition': None,
    'inventory_required': True,
    'filenames': {},
    'patterns': {
        'version': '^.*$',
        'hostname': '^.*$',
        'params': {},
    },

}

def read() -> dict:
    global __CONF
    cwd = '/repos/config'
    with open(os.path.join(cwd, 'bootstrap.yml'), 'r', encoding='utf-8') as f:
        __CONF = yaml.safe_load(f.read())

    # Allow relative paths
    for d in ['template_dir', 'vars_dir', 'defaults']:
        if not os.path.isabs(get(d)):
            __CONF[d] = os.path.join(cwd, get(d))

def get(var: str):
    global __CONF
    global __DEFAULTS
    return __CONF.get(var, __DEFAULTS.get(var))

def precheck(data: dict):
    env = {}
    for e in ['PATH', 'HOME', 'HOSTNAME', 'PWD', 'LANG']:
        env[e] = os.environ.get(e, '')

    log('info', 'Templating var precheck @ /repos/config/bootstrap.yml')
    script = j2.render_str(get('precheck'), data, allow_nonstr=False)

    with subprocess.Popen(
        script,
        stdout = subprocess.PIPE,
        stderr = subprocess.STDOUT,
        env = env,
        shell = True
    ) as process:
        stdout, _ = process.communicate()
        if process.returncode != 0:
            raise HTTPException(
                status_code = status.HTTP_403_FORBIDDEN,
                detail = f'The precheck script denies access to this file: {stdout.decode("utf-8")}'
            )

def add_defaults(data: dict) -> dict:
    if os.path.exists(get('defaults')):
        log('info', f'Templating all vars @ {get("defaults")}')
        with open(get('defaults'), 'r', encoding='utf-8') as f:
            data.update(j2.render_dict(yaml.safe_load(f.read()), data))
    return data

def add_vars(data: dict) -> dict:
    vars_files = []
    log('info', 'Templating var vars_files @ /repos/config/bootstrap.yml')
    vars_files = j2.render_list(get('vars_files'), data)
    for file in vars_files:
        filepath = os.path.join(get('vars_dir'), file)
        if os.path.exists(filepath):
            log('info', f'Templating all vars @ {filepath}')
            with open(filepath, 'r', encoding='utf-8') as f:
                data.update(j2.render_dict(yaml.safe_load(f.read()), data))
    return data

def add_inventory(data: dict) -> dict:
    log('info', 'Templating var inventory_host @ /repos/config/bootstrap.yml')
    host = j2.render_str(get('inventory_host'), data, allow_nonstr=False)
    try:
        inventory = ansible.get_hostvars(
            f'/repos/inventory/{get("inventory_dir")}',
            host,
            get('inventory_condition'),
            data
        )
    except KeyError as e:
        # Host not in inventory
        if get('inventory_required'):
            raise HTTPException(
                status_code = status.HTTP_404_NOT_FOUND,
                detail = str(e),
            ) from e
        inventory = {}
    data.update(inventory)
    return data
