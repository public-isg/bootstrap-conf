from functools import lru_cache
from ansible.inventory.manager import InventoryManager
from ansible.parsing.dataloader import DataLoader
from ansible.vars.manager import VariableManager

from app.config import log
from app.lib import j2
from app.lib import git

@lru_cache(maxsize=1)
def get_inventory(inventory_dir: str, inventory_hash: str):
    log('debug', f'Parsing {inventory_dir} (hash {inventory_hash})')
    loader = DataLoader()
    inv = InventoryManager(loader=loader, sources=inventory_dir)
    vm = VariableManager(loader=loader, inventory=inv)
    return inv, vm

def get_hostvars(inventory_dir: str, host: str, condition: str|None, data: dict):
    inventory_hash = git.get_hash('/repos/inventory')
    log('debug', f'Getting {inventory_dir} (hash {inventory_hash})')
    inv, vm = get_inventory(inventory_dir, inventory_hash)

    hosts = []
    for h in inv.get_hosts(host):
        hostvars = vm.get_vars(host=h, include_hostvars=False)
        if condition is None:
            test = True
        else:
            log('info', 'Templating var inventory_condition @ /repos/config/bootstrap.yml')
            test = bool(j2.render_str(condition, {**data, **hostvars}, allow_nonstr=True))

        log('debug', f'Inventory condition for {h}: {condition} -> {test}')
        if test:
            hosts.append((h, hostvars))

    if len(hosts) != 1:
        raise KeyError(f'Found {len(hosts)} matching hosts in inventory, but must be exactly 1')

    return hosts[0][1]
