import binascii
import datetime
import os
import re
import socket

from fastapi import APIRouter, Request, HTTPException, status
from fastapi.responses import PlainTextResponse

from app.config import env, log
from app.lib import config
from app.lib import j2
from app.router.status import pull_config, pull_inventory

router = APIRouter()

@router.get(
    "/file/{os_name}/{os_version}/{filename:path}",
    summary = "Download a specific file from template",
    response_class = PlainTextResponse(media_type='text/plain'),
    responses = {
        403: {'content': {'application/json': {'schema':
                {'type': 'object', 'properties': {'detail': {'type': 'string'}}}
             }}},
        404: {'content': {'application/json': {'schema':
                {'type': 'object', 'properties': {'detail': {'type': 'string'}}}
             }}},
        500: {'content': {'text/plain': {'schema':
                {'type': 'string'}
             }}},
    },
)
# pylint: disable=too-many-locals,too-many-branches,too-many-statements
def get_file(os_name: str, os_version: str, filename: str, request: Request) -> str:

    # Pull repos and read config

    if env('config_git_autopull'):
        pull_config()
    if env('inventory_git_autopull'):
        pull_inventory()

    config.read()

    # Collect and validate client data

    hostip = request.client.host
    hostip_hex = binascii.hexlify(socket.inet_aton(hostip)).upper()
    hostname = socket.gethostbyaddr(hostip)[0]

    os_names = config.get('filenames').keys()
    if os_name not in os_names:
        raise HTTPException(
            status_code = status.HTTP_404_NOT_FOUND,
            detail = f'The os name is not one of: {", ".join(os_names)}',
        )
    version_pattern = config.get("patterns").get("version")
    if not re.match(version_pattern, os_version):
        raise HTTPException(
            status_code = status.HTTP_404_NOT_FOUND,
            detail = f'The version string does not match: {version_pattern}',
        )
    filenames = config.get('filenames').get(os_name)
    if filename not in filenames:
        raise HTTPException(
            status_code = status.HTTP_404_NOT_FOUND,
            detail = f'The filename is not one of: {", ".join(filenames)}',
        )
    hostname_pattern = config.get('patterns').get('hostname')
    if not re.match(hostname_pattern, hostname):
        raise HTTPException(
            status_code = status.HTTP_403_FORBIDDEN,
            detail = f'Your hostname ({hostname}) does not match: {hostname_pattern}',
        )

    for param in request.query_params.keys():
        params = config.get('patterns').get('params').keys()
        if param not in params:
            raise HTTPException(
                status_code = status.HTTP_403_FORBIDDEN,
                detail = f'Only the following query params are allowed: {", ".join(params)}'
            )
        param_pattern = config.get('patterns').get('params').get(param)
        param_value = request.query_params.get(param)
        if not re.match(param_pattern, param_value):
            raise HTTPException(
                status_code = status.HTTP_403_FORBIDDEN,
                detail = f'The value of query param {param} does not match: {param_pattern}'
            )

    # Build var list

    data = {
        'bootstrap_os_name': os_name,
        'bootstrap_os_version': os_version,
        'bootstrap_filename': filename,
        'bootstrap_hostname': hostname,
        'bootstrap_ip': hostip,
        'bootstrap_ip_hex': hostip_hex,
    }

    for param in request.query_params.keys():
        data[f'bootstrap_param_{param}'] = request.query_params.get(param)

    log('debug', f'IP: {data['bootstrap_ip']}: starting precheck')
    config.precheck(data)

    log('debug', f'IP: {data['bootstrap_ip']}: adding defaults')
    data = config.add_defaults(data)
    log('debug', f'IP: {data['bootstrap_ip']}: adding inventory')
    data = config.add_inventory(data)
    log('debug', f'IP: {data['bootstrap_ip']}: adding vars')
    data = config.add_vars(data)

    # Find template

    template = ''
    template_dir = config.get('template_dir')
    log('info', 'Templating var template_files @ /repos/config/bootstrap.yml')
    template_files = j2.render_list(config.get('template_files'), data)
    for file in template_files:
        if os.path.exists(os.path.join(template_dir, file)):
            template = file
            break

    assert template != ''

    # Update var list

    data.update({
        'bootstrap_template': template,
        'bootstrap_template_dir': template_dir,
        'bootstrap_template_files': template_files,
        'bootstrap_defaults': config.get('defaults'),
        'bootstrap_date': datetime.datetime.today().strftime('%Y-%m-%d'),
        'bootstrap_time': datetime.datetime.today().strftime('%H:%M:%S'),
        # Update webserver variables here to make sure they cannot be overwritten!
        'bootstrap_os_name': os_name,
        'bootstrap_os_version': os_version,
        'bootstrap_filename': filename,
        'bootstrap_hostname': hostname,
        'bootstrap_ip': hostip,
        'bootstrap_ip_hex': hostip_hex,
    })

    for param in request.query_params.keys():
        data[f'bootstrap_param_{param}'] = request.query_params.get(param)

    data.update({'vars': data.copy()})

    # Generate output from template

    log('info', f'Templating {template_dir}/{template}')
    content = j2.render_file(template_dir, template, data)

    # Archive output

    if config.get('template_archive') is not None:
        log('info', 'Templating var template_archive @ /repos/config/bootstrap.yml')
        archive_path = j2.render_str(config.get('template_archive'), data, allow_nonstr=False)
        archive = f'/archive/{archive_path}'
        if not os.path.isdir(os.path.dirname(archive)):
            os.makedirs(os.path.dirname(archive))

        with os.fdopen(os.open(archive, os.O_CREAT | os.O_WRONLY, 0o664), 'w') as fd:
            fd.seek(0)
            fd.truncate(0)
            fd.write(content)

    return PlainTextResponse(content=content, media_type='text/plain')
