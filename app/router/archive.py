import os
from datetime import datetime
from typing import Annotated

from fastapi import APIRouter, Request, Depends
from fastapi.responses import HTMLResponse

from app.lib import auth

router = APIRouter()

def __list_files_r(path: str) -> list:
    result = []
    for root, _, files in os.walk(path):
        rel = os.path.relpath(root, path)
        for file in files:
            mod_ts = os.path.getmtime(os.path.join(root, file))
            mod = datetime.fromtimestamp(mod_ts).strftime('%Y-%m-%d %H:%M:%S')
            result.append((mod, os.path.join(rel, file)))
    return result

@router.get(
    '/archive',
    summary = 'HTML Site with a list of download links for all archived files',
    response_class = HTMLResponse,
    responses = {
        401: {'content': {'text/plain': {'schema': {'type': 'string'}}}},
        500: {'content': {'text/plain': {'schema': {'type': 'string'}}}},
    },
)
def get_archive(request: Request, username: Annotated[str, Depends(auth.verify_secret)]): # pylint: disable=unused-argument
    '''
    This site and all the archived files linked from it require HTTP
    Basic Authentication, where password is BOOTSTRAP_ARCHIVE_SECRET
    (defined via environment) and username is ignored.
    '''

    links = [f'<a data-sort="{t}" href="{request.url}/{f}"><span class="file-icon">📝</span> {t} <span class="file-name">{f}</span></a>' for t, f in __list_files_r('/archive')]
    links_sorted = sorted(links, reverse = True)

    header = '''
        <!DOCTYPE html><html><head><title>Archive</title>
            <style>
            body {
                font-family: Arial, sans-serif;
                font-size: 11px;
                margin: 20px;
                background-color: #f4f4f9;
                color: #333;
            }
            .file-list {
                list-style-type: none;
                padding: 0;
                margin: 0 auto;
                max-width: 600px;
            }
            .file-list li {
                background: #fff;
                margin: 4px 0;
                padding: 5px;
                border-radius: 3px;
                box-shadow: 0 2px 4px rgba(0, 0, 0, 0.1);
                display: flex;
                align-items: center;
                transition: background 0.3s;
            }
            .file-list a {
                text-decoration: none;
                color: #999;
                transition: color 0.3s;
            }
            .file-list li:hover {
                background: #e9f5ff;
            }
            .file-icon {
                margin-right: 2px;
                font-size: 15px;
                color: #007bff;
            }
            .file-name {
                color: #007bff;
            }
        </style></head><body><h1>Archive</h1>'''
    footer = '</body></html>'

    if len(links_sorted) == 0:
        body = '<i>Empty</i>'
    else:
        links_listed = '</li><li>'.join(links_sorted)
        body = f'<ul class="file-list"><li>{links_listed}</li></ul>'

    return f'{header}{body}{footer}'
