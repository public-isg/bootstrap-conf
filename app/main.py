from fastapi import FastAPI

from app.lib.auth import AuthStaticFiles
from app.config import env
from app.router import archive
from app.router import files
from app.router import status

#
# Startup
#

status.pull_config()
status.pull_inventory()

#
# API
#

DESCRIPTION = 'Source and Issues in [GitLab](https://gitlab.inf.ethz.ch/public-isg/bootstrap-conf)'

app = FastAPI(
    title = 'Bootstrap Configuration with Ansible',
    description = DESCRIPTION,
    version = '1.3',
    root_path = '' if env('root_path') == '/' else env('root_path'),
    contact = {
        'name': 'Manuel (isginf)',
        'email': 'manuel.maestinger@inf.ethz.ch',
    },
    license_info = {
        'name': 'GNU GPLv3',
        'url': 'https://www.gnu.org/licenses/gpl-3.0.html',
    },
    docs_url = '/',
    redoc_url = None,
    swagger_ui_parameters = {
        'displayRequestDuration': True,
        'persistAuthorization': True
    },
)

app.include_router(status.router, tags=['Status'])
app.include_router(files.router, tags=['Files'])
app.include_router(archive.router, tags=['Archive'])

app.mount('/archive', AuthStaticFiles(directory='/archive'), name='archive')
# TODO add templating stack traces
# TODO oidc auth
